import torch

from diffusers import StableDiffusionPipeline, EulerDiscreteScheduler
from naogi import NaogiModel, PilImageRenderer

class Model(NaogiModel):
    def init_model(self):
        pass


    def load_model(self):
        pass


    def prepare(self, params):
        self.prompt = str(params['prompt'])


    def predict(self):
        model_id = "stabilityai/stable-diffusion-2"

        scheduler = EulerDiscreteScheduler.from_pretrained(model_id, subfolder="scheduler")
        pipe = StableDiffusionPipeline.from_pretrained(model_id, scheduler=scheduler, revision="fp16", torch_dtype=torch.float16)
        pipe = pipe.to("cuda")

        return pipe(self.prompt, height=768, width=768).images[0]


    def renderer(self):
        return PilImageRenderer

